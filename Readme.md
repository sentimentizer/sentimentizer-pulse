# Readme
Pulse is a jQuery Mobile page that will look up the current vote sentiment (an average of the votes: Add the positive and negatives and divide by the votes) for a given topic and render it in a graph. It accesses the data via DynamoDB calls.

#Install
Designed to run in S3 as a static web page - it downloads all of its dependencies from the client side "live". To install, just push the HTML file up to the relevant S3 bucket, and do any R53 CNAMEing as required.

A read only IAM user is required for access to DynamoDB tables as follows:
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1389520428000",
      "Effect": "Allow",
      "Action": [
        "dynamodb:BatchGetItem",
        "dynamodb:DescribeTable",
        "dynamodb:GetItem",
        "dynamodb:Query",
        "dynamodb:Scan"
      ],
      "Resource": [
        "arn:aws:dynamodb:ap-southeast-2:581371153492:table/Sentimentizer*"
      ]
    }
  ]
}
```

#How to Use
Calls to this page need to include 2 Query Parameters in order to render.

```
?topic=<topiccode>&desc=<topic description to display>
```
For example:
```
http://pulse.dcin5.com?topic=TopicA&desc=Topic A Description
```
If you do not specify a topic, the graph will just not render.